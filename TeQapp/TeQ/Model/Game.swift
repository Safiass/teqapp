
//  Game.swift
//  TeQ

//// Il s'agit de la classe Game qui a comme propriétés: (1), (2), (3), (4)
import Foundation

/// énumération//////////////////////////////////////////////////////////////////////////////////////////////////
enum State {
    case ongoing, /// partie prête à démarrer
    over, wining /// partie finie
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class Game {
    
    var score = 0 // (1) le score du joueur initialisé à 0
    private var questions = [Question]() // (2) tableau des questions qui ont comme type structure "Question"
    private var currentIndex = 0 //(3)
    var state: State = .ongoing // (4) variable de type énumération "State" qui comme valeur .ongoing
    var currentQuestion: Question { return questions[currentIndex]} // (5) déclaration  variable de type structure Question/////////////
    
    
//////// Commencer le jeu à zéro: Initialisation /////////////////////////////////////////////////////////////////////
    func refresh() {
        score = 0
        currentIndex = 0
        state = .over
        QuestionManager.shared.get { (questions) in
            self.questions = questions
            self.state = .ongoing
            let name = Notification.Name(rawValue: "QuestionsLoaded")
            let notification = Notification(name: name)
            NotificationCenter.default.post(notification)
           }
    }
   
    
////////// Dans ce cas on augmente le score par 1 en comparant la réponse de l'utilisateur et la valeur définit par "isCorrect"
    func answerCurrentQuestion(with answer: Bool) {
        if (currentQuestion.isCorrect && answer) || (!currentQuestion.isCorrect && !answer) {
            score += 1
        }
        goToNextQuestion()
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
//////// Passer à la prochaine question en faisant une comparaison d'index et le nombre des questions.///////////////
    private func goToNextQuestion() {
        if currentIndex < questions.count - 1 {
            currentIndex += 1
        } else {
            finishGame()
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
/////// état lorsqu'on finit une question///////////////////////////////////////////////////////////////////////////
    private func finishGame() {
        if( score <= 3 && score <= questions.count){
            state = .over
        } else {
            state = .wining
        }
        
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
}
