
//  Question.swift
//  TeQ

/////// Il s'agit de la structure Question qui a comme propriété: title de type String et isCorrect de type boolean

import Foundation

struct Question{
    var title = ""
    var isCorrect = false
    
}
