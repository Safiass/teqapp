//
//  QuestionView.swift
//  TeQ
//

/// This class concern the view that contains the label where the question will be shown as well as the icon

import UIKit
// Enumération: correct is the answer is correct, incorrect if not and finally standard if there is no answer

enum Style {
    case correct, incorrect, standard
}

class QuestionView: UIView {
    @IBOutlet  var label: UILabel!
    @IBOutlet private var icon: UIImageView!
    
    var style: Style = .standard {
        didSet {                     //didSet est appelé après la modification de la valeur style (devient standard)
            setStyle(_style: style)
        }
    }
    
    var title = "" {
        didSet {
            label.text = title
        }
    }

/// Function so as to change the style of the view according to the enumeration cases. ////////////////////////////
  private func setStyle(_style: Style) {
        switch style {
        case .correct:
            backgroundColor = UIColor(red: 200.0/255.0, green: 236.0/255.0, blue: 160.0/255.0, alpha: 1) // Vert
            icon.image = UIImage(named: "Icon Correct")
            icon.isHidden = false
        case .incorrect:
            backgroundColor = UIColor(red: 243.0/255.0, green: 135.0/255.0, blue: 148.0/255.0, alpha: 1) // Rouge
            icon.image = UIImage(named: "Icon Error")
            icon.isHidden = false
        case .standard:
             backgroundColor = UIColor(red: 191.0/255.0, green: 196.0/255.0, blue: 201.0/255.0, alpha: 1) // Gris
             icon.isHidden = true
            }
        }
    
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
