//
//  InitialController.swift
//  TeQ
//


import UIKit

extension UIButton {
    func animatebutton(){
        let shake = CABasicAnimation(keyPath: "position")
        shake.duration = 0.1
        shake.repeatCount = 2
        shake.autoreverses = true
        
        let fromPoint = CGPoint(x: center.x - 5, y: center.y)
        let fromValue = NSValue(cgPoint: fromPoint)
        
        let toPoint = CGPoint(x: center.x + 5, y: center.y)
        let toValue = NSValue(cgPoint: toPoint)
        
        shake.fromValue = fromValue
        shake.toValue = toValue
        
        layer.add(shake, forKey: nil)
        
    }
}

class InitialController: UIViewController {
    
    @IBOutlet weak var welcomeImage: UIImageView!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var exitButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        animateImage(image: welcomeImage, minusY: -50, plusY: +50)
        }
    
    public func animateImage(image:UIImageView, minusY: CGFloat, plusY: CGFloat) {
        UIView.animate(withDuration: 1, animations: {
            image.frame.origin.y -=  minusY
        }) { _ in
            UIView.animateKeyframes(withDuration: 1, delay: 0.25, options: [.autoreverse, .repeat], animations: {
            image.frame.origin.y += plusY
            })
        }
}
    
    @IBAction func startTheGame(_ sender: Any) {
        startButton.animatebutton()
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            self.performSegue(withIdentifier: "segue", sender: self)
        })
        
    }
    
    @IBAction func exitGame(_ sender: Any) {
        exitButton.animatebutton()
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            exit(0)
        })
    }
    
}


