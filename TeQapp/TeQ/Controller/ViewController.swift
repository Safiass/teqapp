//  ViewController.swift
//  TeQ
//


import UIKit

class ViewController: UIViewController {
    
   
    var game = Game() // déclaration de l'objet game de type Game

    @IBOutlet weak var newGameButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet var questionView: QuestionView!
    
    @IBOutlet weak var resultImage: UIImageView!
    
    @IBOutlet weak var imagescore: UIImageView!
    
    ////////viewDidLoad /////////////////////////////////////////////////////////////////////////////////////////
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        let name = Notification.Name(rawValue: "QuestionsLoaded")
        NotificationCenter.default.addObserver(self, selector: #selector(questionsLoaded), name: name, object: nil)
        startNewGame()
        //detect dragging gestures
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(dragQuestionView(_:)))
        questionView.addGestureRecognizer(panGestureRecognizer) // Reconnaisance du geste
        }
////////////////////////////////////////////////////////////////////////////////////////////////////////
    
/////////Héritage Objective-C pour passer dans le selector ////////////////////////// question chargée
    @objc func questionsLoaded() {
        activityIndicator.isHidden = true
        newGameButton.isHidden = false
        questionView.title = game.currentQuestion.title
        }
////////////////////////////////////////////////////////////////////////////////////////////////////////
    
////// Ici quand l'utilisateur séléctionne le button "New Game"
    @IBAction func tapeNewGame(_ sender: Any) {
        startNewGame()
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////
    
//////// to start a  new game /////////////////////////////////////////////////////////////////////////
    func startNewGame(){ //chargement de la question
        
        newGameButton.isHidden = true
        activityIndicator.isHidden = false
        scoreLabel.text = "0/10"
        questionView.title = "Loading Question ..."
        questionView.style = .standard
        game.refresh() // appel à la méthode pour le début de jeu
        resultImage.isHidden = true
        
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////
  
///////////// fonction qui réagit selon le mouvement de question view, de began ... cancelled //////////
    @objc func dragQuestionView(_ sender: UIPanGestureRecognizer) {
        if game.state == .ongoing { // function active ssi on est au cours du jeu
            switch sender.state {
            case .began, .changed: // geste en cours
                transformQuestionViewWith(gesture: sender)
            case .ended, .cancelled: // geste est terminé
                answerQuestion()
            default:
                break
        }
        }
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
//////////// Geste passé dans cette méthode ///////////////////////////////////////////////////////////
    private func transformQuestionViewWith(gesture: UIPanGestureRecognizer){
        
        let translation = gesture.translation(in: questionView) // translation de la vue questionView récupéré par le doigt
        
        //Utilisation de l'information translation pour déplacer la vue
        let translationTransform = CGAffineTransform(translationX: translation.x, y: translation.y)// jusque là, on remarque que la questionView suit la souris // CGAffineTransform sert à créer une translation
        
        let translationPercent = translation.x/(UIScreen.main.bounds.width/2) // entre -100% et 100%
        let rotationAngle = (CGFloat.pi/6)*translationPercent // angle de rotation
        let rotationTransform = CGAffineTransform(rotationAngle: rotationAngle)// transformation en rotation
        let transform = translationTransform.concatenating(rotationTransform)// concatenation de la translation et la rotation
        questionView.transform = transform // affectation de la transformation à questionView
        
        // Changement du style selon l'angle de rotation
        if translation.x>0{
            questionView.style = .correct
        } else {
            questionView.style = .incorrect
        }
        
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
//////////// Vérification de la réponse juste ou pas ///////////////////////////////////////////////////////////
    private func answerQuestion() {
        // ici on envoie la réponse au modèle "Game"
        switch questionView.style {
        case .correct :
            game.answerCurrentQuestion(with: true)
        case .incorrect:
            game.answerCurrentQuestion(with: false)
        case .standard:
            break
        }
        
        scoreLabel.text = "\(game.score)/10"
        
        // Place à l'animation :D //////////////////////////////////////////////////////////////////////
        let screenWidth = UIScreen.main.bounds.width
        var translationTransform: CGAffineTransform// translation
        if questionView.style == .correct {
            translationTransform = CGAffineTransform(translationX: screenWidth, y: 0)
        } else {
            translationTransform = CGAffineTransform(translationX: -screenWidth, y: 0)
        }
        
        UIView.animate(withDuration: 0.3, animations: {
            self.questionView.transform = translationTransform // on applique la translation
        }) { (success) in
            self.showQuestionView()
        }
    }
 ///////////////////////////////////////////////////////////////////////////////////////////////////////
    
//////////// Après avoir répondu à une question à un moment T ///////////////////////////////////////////////////////////
    private func showQuestionView() {
        questionView.transform = .identity// on revient au centre
        questionView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)// on réduit la taille (très petite)
        questionView.style = .standard
        
        /// cas où on arrive à la dixième question(.over)
        switch game.state {
        case .ongoing:
            questionView.title = game.currentQuestion.title
        case .over:
            questionView.title = "Game Over"
            resultImage.image = UIImage(named: "youlose")
            animateImage(image: resultImage, minusY: 100, plusY: 300)
            resultImage.isHidden = false
            questionView.isHidden = true
            
        case .wining:
            questionView.title = "You Win"
            resultImage.image = UIImage(named: "youwin")
            animateImage(image: resultImage, minusY: 100, plusY: 300)
            resultImage.isHidden = false
            
        }
        ///Animation
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: [], animations: {
            self.questionView.transform = .identity
        }, completion:nil)
    }
   
    @IBAction func returnAction(_ sender: AnyObject) {
        let initialController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "initialController") as? InitialController
        present(initialController!, animated: true, completion: nil)

    }
    
    public func animateImage(image:UIImageView, minusY: CGFloat, plusY: CGFloat) {
        UIView.animate(withDuration: 1, animations: {
            image.frame.origin.y -=  minusY
        }) { _ in
            UIView.animateKeyframes(withDuration: 1, delay: 0.25, options: [.autoreverse, .repeat], animations: {
                image.frame.origin.y += plusY
            })
        }
    }

    

}

