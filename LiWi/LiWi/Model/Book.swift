//
//  Book.swift
//  LiWi
//
//  Created by Safia CHMITI on 11/18/18.
//  Copyright © 2018 Safia CHMITI. All rights reserved.
//

import Foundation

struct Book {
    var title = ""
    var author = ""
}

extension Book: Present {
    var icon: String {
        return "books"
    }
    
    var description: String {
        return title
    }
    
    var detail: String {
        return author
    }
    
    
}
