//
//  Present.swift
//  LiWi
//


import Foundation

protocol Present {
    var description: String { get }
    var detail: String { get }
    var icon: String { get }
}
