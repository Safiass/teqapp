//
//  Trip.swift
//  LiWi
//
//  Created by Safia CHMITI on 11/18/18.
//  Copyright © 2018 Safia CHMITI. All rights reserved.
//

import Foundation

struct Trip {
    var departure = ""
    var destination = ""
    var spentday = 0
}

extension Trip: Present {
    var icon: String {
        return "travel"
    }
    
    var description: String {
        return "\(departure) -> \(destination)"
    }
    
    var detail: String {
        return "\(spentday)"
    }
    
    
}
