//
//  ToyService.swift
//  LiWi
//


import Foundation

class ToyService {
    static let shared = ToyService()
    private init() {}
    
    private(set) var toys: [Present] = []
    
    func add(toy: Present) { // Ajouter un jouet dans le tableau Toy 
        toys.append(toy)
    }
}


