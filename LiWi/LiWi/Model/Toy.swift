//
//  Toy.swift
//  LiWi
//


import Foundation

struct Toy {
    var name = ""
    var brand = ""
}

extension Toy: Present {
    var icon: String {
        return "puzzle"
    }
    
    var description: String {
       return name
    }
    
    var detail: String {
        return brand
    }
    
    
}
