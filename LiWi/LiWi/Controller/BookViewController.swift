//
//  BookViewController.swift
//  LiWi
//

import UIKit

class BookViewController: UIViewController {

    @IBOutlet weak var titleTextField: UITextField!
    
    @IBOutlet weak var authorTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
}

    @IBAction func saveBook(_ sender: Any) {
        guard let title = titleTextField.text,
            let author = authorTextField.text else {
                return
        }
        
        let book = Book(title: title, author: author)
        ToyService.shared.add(toy: book)
        performSegue(withIdentifier: "booksegue", sender: nil)
        
    }
    
    
}


extension BookViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
