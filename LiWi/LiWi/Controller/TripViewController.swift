//
//  TripViewController.swift
//  LiWi
//


import UIKit

class TripViewController: UIViewController {

    @IBOutlet weak var departureTextField: UITextField!
    @IBOutlet weak var destinationTextField: UITextField!
    @IBOutlet weak var daysStep: UIStepper!
    @IBOutlet weak var dayspentLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
}

    @IBAction func saveTrip(_ sender: Any) {
        guard let departure = departureTextField.text,
            let destination = destinationTextField.text else {
                return
        }
        
    let trip = Trip(departure: departure, destination: destination,spentday: Int(daysStep.value))
    ToyService.shared.add(toy: trip)
    performSegue(withIdentifier: "tripSegue", sender: nil)
    }
    
    
    @IBAction func changeSpentDays(_ sender: UIStepper) {
        let valuestepper = Int(daysStep.value)
        dayspentLabel.text = "\(valuestepper) days"
    }
    
    
    

}

extension TripViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
