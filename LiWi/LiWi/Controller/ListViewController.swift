//
//  ListViewController.swift
//  LiWi
//


import UIKit

class ListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func unwindToListVC(segue: UIStoryboardSegue) {}


    
    override func viewDidLoad() {
        super.viewDidLoad()
}

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
   

}

extension ListViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ToyService.shared.toys.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PresentCell", for: indexPath) as! PresentTableViewCell
        let toy = ToyService.shared.toys[indexPath.row]
        cell.configure(withIcon: toy.icon, title: toy.description, subtitle: toy.detail)
        
        return cell
    }
    
    
}
