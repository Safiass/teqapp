//
//  ToyViewController.swift
//  LiWi
//

import UIKit

class ToyViewController: UIViewController{
   
   
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var brandTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        }
    
    @IBAction func addToy(_ sender: Any) {
        guard let name = nameTextField.text,
            let brand = brandTextField.text else {
                return
        }
        
        let toy = Toy(name: name, brand: brand)
        ToyService.shared.add(toy: toy)
        performSegue(withIdentifier: "toysegue", sender: nil)
    }
}

extension ToyViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
