//
//  PresentTableViewCell.swift
//  LiWi
//
//  Created by Safia CHMITI on 11/18/18.
//  Copyright © 2018 Safia CHMITI. All rights reserved.
//

import UIKit

class PresentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var descriptionTextField: UILabel!
    @IBOutlet weak var detailTextField: UILabel!
    @IBOutlet weak var cellView: UIView!
    
    
//cette méthode est utilisée lorsqu'une vue est initialisée depuis un storyboard. C'est ici où on peut customiser la cellule
    override func awakeFromNib() {
        super.awakeFromNib()
        addShadow()
        
    }

    private func addShadow() {
        cellView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7).cgColor
        cellView.layer.shadowRadius = 2.0
        cellView.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        cellView.layer.shadowOpacity = 2.0
    }
   
    func configure(withIcon: String, title: String, subtitle: String) {
        iconImage.image = UIImage(named: withIcon)
        descriptionTextField.text = title
        detailTextField.text = subtitle
    }
}
